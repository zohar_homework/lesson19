﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.IO;
using System.Text;

namespace qw1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Dictionary<string,string> pairs = new Dictionary<string,string>();
            // Open the stream and read it back.
            using (StreamReader fs = new StreamReader("Users.txt"))
            {
                byte[] b = new byte[1024];
                UTF8Encoding temp = new UTF8Encoding(true);
                int user_name=0;
                int user_password=0;
                String line = fs.ReadToEnd();
                string []  match=line.Split('\n');
                foreach(string s in match)
                {
                    string [] pair = s.Split(',');
                    pairs.Add(pair[0],pair[1]);
                    int checkN=string.Compare(textBox1.Text,pair[0]);
                    if(checkN==0)
                    {
                        user_name=1;
                    }
                    if ( user_name==1)
                    {
                        int checkP=string.Compare(textBox2.Text,pair[1]);
                        if(checkP==0)
                        {
                            user_password=1;
                        }

                    }
                }
                if(user_name==0)
                {
                    Console.WriteLine("The name does not exist in the system");
                }
                if(user_password==0)
                {
                    Console.WriteLine("The password does not exist in the system");
                }
                if(user_name==1 && user_password==1)
                {
                    this.Hide();
                    form2 Form2 = new form2(textBox1.Text);
                    Form2.ShowDialog();
                    this.Close();

                } 
           
            }
        }
      
    }
}
